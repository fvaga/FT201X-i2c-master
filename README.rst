FTDI provides a generic driver that gives access to the USB interface.
This can be used to write userspace driver easily without any
kernel developlment.
This approach has at list one important limitation. The users can't
use the I2C drivers for those devices connected to the FT201X I2C master.

This driver provides a Linux I2C master driver for the FT201X chip.
